package com.infinitemind.fiszkomania.models;

import java.util.ArrayList;

public class Set {

	private ArrayList<Question> questions;
	private String title;
	private int color;

	public Set() { }

	public Set(String title, int color) {
		this.title = title;
		this.color = color;
		this.questions = new ArrayList<>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public ArrayList<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(ArrayList<Question> questions) {
		this.questions = questions;
	}
}
