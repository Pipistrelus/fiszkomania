package com.infinitemind.fiszkomania.models;

public class Color {

	private boolean checked;
	private int color;

	public Color(boolean checked, int color) {
		this.checked = checked;
		this.color = color;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}
}
