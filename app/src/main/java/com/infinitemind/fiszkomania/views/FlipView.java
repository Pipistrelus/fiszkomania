package com.infinitemind.fiszkomania.views;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

import com.infinitemind.fiszkomania.interfaces.OnAnimationEndListener;

public class FlipView extends FrameLayout {

	private static final int DEFAULT_DURATION = 200;
	private boolean frontShowed = true;
	private SetCardView front, back;

	public FlipView(@NonNull Context context) {
		super(context);
		init();
	}

	public FlipView(@NonNull Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public FlipView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	private void init() {
		post(() -> {
			if(getChildCount() != 2) throw new RuntimeException("You have to have two sides!");

			front = (SetCardView) getChildAt(0);
			back = (SetCardView) getChildAt(1);

			back.setVisibility(INVISIBLE);
		});
	}

	public void flipView(int duration) {
		ObjectAnimator r1 = ObjectAnimator.ofFloat(frontShowed ? front : back, "rotationY", (frontShowed ? front : back).getRotationY(), frontShowed ? -90 : 90);
		r1.setDuration(duration);
		r1.setInterpolator(new DecelerateInterpolator(2f));
		r1.start();

		r1.addListener(new OnAnimationEndListener(a1 -> {
			(frontShowed ? back : front).setVisibility(VISIBLE);
			ObjectAnimator r2 = ObjectAnimator.ofFloat(frontShowed ? back : front, "rotationY", frontShowed ? 90 : -90, 0);
			r2.setDuration(duration);
			r2.setInterpolator(new DecelerateInterpolator(2f));
			r2.start();

			r2.addListener(new OnAnimationEndListener(a2 -> ((frontShowed = !frontShowed) ? back : front).setVisibility(INVISIBLE)));
		}));
	}

	public void flipView() {
		flipView(DEFAULT_DURATION);
	}

	public void setFrontShowed(boolean frontShowed) {
		this.frontShowed = frontShowed;
		if(frontShowed) {
			front.setVisibility(VISIBLE);
			back.setVisibility(INVISIBLE);
			front.setRotationY(0);
		} else {
			front.setVisibility(INVISIBLE);
			back.setVisibility(VISIBLE);
			back.setRotationY(0);
		}
	}

	public boolean isFrontShowed() {
		return frontShowed;
	}

	public SetCardView getFront() {
		return front;
	}

	public SetCardView getBack() {
		return back;
	}
}
