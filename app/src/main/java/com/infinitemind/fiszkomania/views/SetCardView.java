package com.infinitemind.fiszkomania.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.infinitemind.fiszkomania.R;

public class SetCardView extends FrameLayout {

	private String title;
	private String subtitle;
	private int image;
	private int color;

	public SetCardView(@NonNull Context context) {
		super(context);
		init(null);
	}

	public SetCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public SetCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(attrs);
	}

	private void init(@Nullable AttributeSet attrs) {
		Context context = getContext();

		addView(inflate(context, R.layout.card_view, null));

		if(attrs != null) {
			TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SetCardView, 0, 0);

			try {
				title = a.getString(R.styleable.SetCardView_cv_title);
				subtitle = a.getString(R.styleable.SetCardView_cv_subtitle);
				image = a.getResourceId(R.styleable.SetCardView_cv_image, R.mipmap.ic_launcher);
				color = a.getColor(R.styleable.SetCardView_cv_color, getResources().getColor(R.color.colorAccent));
			} catch(Exception ignored) {}

			a.recycle();

			((AppCompatTextView) findViewById(R.id.title)).setText(title);
			((AppCompatTextView) findViewById(R.id.subtitle)).setText(subtitle);
			((ImageView) findViewById(R.id.image)).setImageResource(image);
			((ImageView) findViewById(R.id.image)).setColorFilter(color);
			((ImageView) findViewById(R.id.background)).setColorFilter(color);
		}
	}

	public int getColor() {
		return color;
	}

	public void setColor(@ColorInt int color) {
		this.color = color;
		((ImageView) findViewById(R.id.image)).setColorFilter(color);
		((ImageView) findViewById(R.id.background)).setColorFilter(color);
	}

	public int getImage() {
		return image;
	}

	public void setImage(@DrawableRes int image) {
		this.image = image;
		((ImageView) findViewById(R.id.image)).setImageResource(image);
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
		((AppCompatTextView) findViewById(R.id.subtitle)).setText(subtitle);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		((AppCompatTextView) findViewById(R.id.title)).setText(title);
	}
}
