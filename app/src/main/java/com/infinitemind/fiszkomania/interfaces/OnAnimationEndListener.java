package com.infinitemind.fiszkomania.interfaces;

import android.animation.Animator;

public class OnAnimationEndListener implements Animator.AnimatorListener {

	private DataRunnable<Animator> callback;

	public OnAnimationEndListener(DataRunnable<Animator> callback) {
		this.callback = callback;
	}

	@Override
	public void onAnimationStart(Animator animation) {

	}

	@Override
	public void onAnimationEnd(Animator animation) {
		callback.run(animation);
	}

	@Override
	public void onAnimationCancel(Animator animation) {

	}

	@Override
	public void onAnimationRepeat(Animator animation) {

	}
}
