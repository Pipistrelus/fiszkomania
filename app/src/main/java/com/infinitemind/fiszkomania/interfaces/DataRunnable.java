package com.infinitemind.fiszkomania.interfaces;

public interface DataRunnable<T> {
	void run(T data);
}
