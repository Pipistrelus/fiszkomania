package com.infinitemind.fiszkomania.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;

public class Utils {

	public static int dpToPx(Context context, int dp) {
		if(context != null) return (int) (context.getResources().getDisplayMetrics().density * dp);
		else return 0;
	}

	public static Point getScreenSize(Activity activity) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		return new Point(displayMetrics.widthPixels, displayMetrics.heightPixels);
	}

}
