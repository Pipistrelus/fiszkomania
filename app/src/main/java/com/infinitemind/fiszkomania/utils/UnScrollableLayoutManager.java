package com.infinitemind.fiszkomania.utils;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

public class UnScrollableLayoutManager extends LinearLayoutManager {

	public UnScrollableLayoutManager(Context context, int orientation, boolean reverseLayout) {
		super(context, orientation, reverseLayout);
	}

	@Override
	public boolean canScrollVertically() {
		return false;
	}
}
