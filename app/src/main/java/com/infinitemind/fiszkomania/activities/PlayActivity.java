package com.infinitemind.fiszkomania.activities;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.infinitemind.fiszkomania.R;
import com.infinitemind.fiszkomania.interfaces.OnAnimationEndListener;
import com.infinitemind.fiszkomania.models.Question;
import com.infinitemind.fiszkomania.models.Set;
import com.infinitemind.fiszkomania.utils.Utils;
import com.infinitemind.fiszkomania.views.FlipView;

import java.util.Random;

public class PlayActivity extends Activity {

	private FlipView currentCard;
	private int questionNumber;
	private Set currentSet;
	public int width;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play);

		String set = getIntent().getStringExtra("set");
		if(set == null || set.isEmpty()) finish();

		width = Utils.getScreenSize(this).x;

		currentSet = new Gson().fromJson(set, new TypeToken<Set>(){}.getType());

		init();
		initCards();
	}

	private void init() {
		((AppCompatTextView) findViewById(R.id.topBarTitle)).setText(currentSet.getTitle());
		((AppCompatTextView) findViewById(R.id.topBarProgress)).setText(getResources().getString(R.string.progress, questionNumber, currentSet.getQuestions().size()));
		ProgressBar progressBar = findViewById(R.id.topBarProgressBar);
		progressBar.setMax(currentSet.getQuestions().size());
		progressBar.setProgress(questionNumber);
	}

	private void initCards() {
		findViewById(R.id.cardView2).setX(width);

		currentCard = findViewById(R.id.cardView1);
		currentCard.post(this::setNextQuestion);
	}

	private Question getNextQuestion() {
		return currentSet.getQuestions().get(new Random().nextInt(currentSet.getQuestions().size()));
	}

	private void setNextQuestion() {
		((ProgressBar) findViewById(R.id.topBarProgressBar)).setProgress(questionNumber);
		((AppCompatTextView) findViewById(R.id.topBarProgress)).setText(getResources().getString(R.string.progress, questionNumber, currentSet.getQuestions().size()));

		if(questionNumber == currentSet.getQuestions().size()) {
			finish();
			Toast.makeText(getApplicationContext(), "Yeey. This is the end", Toast.LENGTH_LONG).show();
		} else {
			Question q = getNextQuestion();
			currentCard.getFront().setColor(currentSet.getColor());
			currentCard.getBack().setColor(currentSet.getColor());

			currentCard.getFront().setTitle(getResources().getString(R.string.question_number, ++questionNumber));
			currentCard.getBack().setTitle(getResources().getString(R.string.answer));

			currentCard.getFront().setSubtitle(q.getQuestion());
			currentCard.getBack().setSubtitle(q.getAnswer());
		}
	}

	private void clickNext() {
		FlipView previousCard = currentCard;
		currentCard = findViewById(currentCard.getId() != R.id.cardView1 ? R.id.cardView1 : R.id.cardView2);
		currentCard.setFrontShowed(true);
		currentCard.setVisibility(View.VISIBLE);
		ObjectAnimator tX_1 = ObjectAnimator.ofFloat(previousCard, "translationX", 0, -width);
		ObjectAnimator tX_2 = ObjectAnimator.ofFloat(currentCard, "translationX", width, 0);
		AnimatorSet set = new AnimatorSet();
		set.playTogether(tX_1, tX_2);
		set.setDuration(400);
		set.setInterpolator(new DecelerateInterpolator(2f));
		set.addListener(new OnAnimationEndListener(a -> {
			previousCard.setVisibility(View.GONE);
			previousCard.setFrontShowed(true);
		}));
		set.start();

		setNextQuestion();


		findViewById(R.id.button2).setVisibility(View.GONE);
		((AppCompatTextView) findViewById(R.id.textButton1)).setText(R.string.show_answer);
	}

	public void clickFlip(View view) {
		if(currentCard.isFrontShowed()) {
			currentCard.flipView();
			findViewById(R.id.button2).setVisibility(View.VISIBLE);
			((AppCompatTextView) findViewById(R.id.textButton1)).setText(R.string.not_know);
		} else clickNext();
		TransitionManager.beginDelayedTransition(findViewById(R.id.buttons), new AutoTransition());
	}

	public void clickNotKnow(View view) {
		clickNext();
	}
}
