package com.infinitemind.fiszkomania.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewTreeObserver;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.infinitemind.fiszkomania.R;
import com.infinitemind.fiszkomania.activities.AddSetActivity;
import com.infinitemind.fiszkomania.activities.PlayActivity;
import com.infinitemind.fiszkomania.adapters.SetsListAdapter;
import com.infinitemind.fiszkomania.models.Set;
import com.infinitemind.fiszkomania.utils.Utils;

import java.util.ArrayList;

public class MainActivity extends Activity {

	private ArrayList<Set> sets;
	public RecyclerView setsList;
	public SetsListAdapter setsListAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		setSetsList();
	}

	private void setSetsList() {
		int width = Utils.getScreenSize(this).x / 2;
		setsList = findViewById(R.id.setList);
		setsList.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
		setsList.setAdapter(setsListAdapter = new SetsListAdapter(width, sets = getSets()));
		setsListAdapter.setOnSetClickListener(position -> {
			if(position == setsListAdapter.getItemCount() - 1)
				startActivity(new Intent(this, AddSetActivity.class));
			else startActivity(new Intent(this, PlayActivity.class).putExtra("set", new Gson().toJson(sets.get(position))));
		});

		setsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);
				((CardView) findViewById(R.id.topBar)).setCardElevation(recyclerView.computeVerticalScrollOffset() > 0 ? Utils.dpToPx(getApplicationContext(), 2) : 0);
			}
		});
	}

	private ArrayList<Set> getSets() {
		ArrayList<Set> sets = new ArrayList<>();
		SharedPreferences sP = getSharedPreferences("Sets", MODE_PRIVATE);
		String set_0 = sP.getString("set_0", "");
		if(!set_0.isEmpty()) sets.add(new Gson().fromJson(set_0, new TypeToken<Set>(){}.getType()));
		sets.add(new Set("", getResources().getColor(R.color.colorAccent)));
		return sets;
	}

	@Override
	protected void onResume() {
		super.onResume();
		sets.clear();
		sets.addAll(getSets());
		setsListAdapter.notifyDataSetChanged();
	}
}
