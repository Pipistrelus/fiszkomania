package com.infinitemind.fiszkomania.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;

import com.google.gson.Gson;
import com.infinitemind.fiszkomania.R;
import com.infinitemind.fiszkomania.adapters.QuestionsListAdapter;
import com.infinitemind.fiszkomania.adapters.SetColorsListAdapter;
import com.infinitemind.fiszkomania.models.Color;
import com.infinitemind.fiszkomania.models.Question;
import com.infinitemind.fiszkomania.models.Set;
import com.infinitemind.fiszkomania.utils.UnScrollableLayoutManager;
import com.infinitemind.fiszkomania.utils.Utils;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class AddSetActivity extends Activity {

	private QuestionsListAdapter questionsListAdapter;
	private SetColorsListAdapter setColorsListAdapter;
	private ArrayList<Question> questions;
	private ArrayList<Color> colors;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_set);

		init();
		setQuestionsList();
		setColorsList();
	}

	private void init() {
		findViewById(R.id.scrollView).getViewTreeObserver().addOnScrollChangedListener(() ->
				findViewById(R.id.scrollView).post(() -> ((CardView) findViewById(R.id.topBar)).setCardElevation(
						findViewById(R.id.scrollView).getScrollY() > 0 ? Utils.dpToPx(getApplicationContext(), 2) : 0)));
	}

	private void setQuestionsList() {
		((RecyclerView) findViewById(R.id.questionsList)).setLayoutManager(new UnScrollableLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
		((RecyclerView) findViewById(R.id.questionsList)).setAdapter(questionsListAdapter = new QuestionsListAdapter(questions = new ArrayList<>()));
	}

	private void setColorsList() {
		((RecyclerView) findViewById(R.id.setColorList)).setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
		((RecyclerView) findViewById(R.id.setColorList)).setAdapter(setColorsListAdapter = new SetColorsListAdapter(colors = getColors()));
		setColorsListAdapter.setOnColorClickListener(position -> {
			for(int i = 0; i < colors.size(); i++)
				colors.get(i).setChecked(false);
			colors.get(position).setChecked(true);
			setColorsListAdapter.notifyDataSetChanged();
		});
	}

	public void clickSave(View view) {
		Set set = new Set();
		set.setTitle(((AppCompatEditText) findViewById(R.id.title)).getText().toString());
		for(Color c : colors) if(c.isChecked()) set.setColor(c.getColor());
		set.setQuestions(questions);

		SharedPreferences.Editor sP = getSharedPreferences("Sets", MODE_PRIVATE).edit();
		sP.putString("set_0", new Gson().toJson(set)).apply();
		finish();
	}

	public void clickAddQuestion(View view) {
		String question = ((AppCompatEditText) findViewById(R.id.question)).getText().toString();
		String answer = ((AppCompatEditText) findViewById(R.id.answer)).getText().toString();
		((AppCompatEditText) findViewById(R.id.question)).setText("");
		((AppCompatEditText) findViewById(R.id.answer)).setText("");
		questions.add(new Question(question, answer));
		questionsListAdapter.notifyDataSetChanged();
	}

	private ArrayList<Color> getColors() {
		Context context = getApplicationContext();
		ArrayList<Color> colors = new ArrayList<>();
		Field[] fields = R.color.class.getFields();
		String[] stringNames = new String[fields.length];
		for(int i = 0; i < fields.length; i++) {
			stringNames[i] = fields[i].getName();
			if(stringNames[i].startsWith("color")) {
				int color = context.getResources().getColor(context.getResources().getIdentifier(stringNames[i], "color", context.getPackageName()));
				if(color != android.graphics.Color.WHITE)
					colors.add(new Color(false, color));
			}
		}
		colors.get(0).setChecked(true);
		return colors;
	}
}
