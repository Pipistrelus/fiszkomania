package com.infinitemind.fiszkomania.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.infinitemind.fiszkomania.R;
import com.infinitemind.fiszkomania.models.Color;

import java.util.ArrayList;

public class SetColorsListAdapter extends RecyclerView.Adapter<SetColorsListAdapter.DataObjectHolder> {
	private OnColorClickListener onColorClickListener;
	private ArrayList<Color> colors;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		ImageView image, check;
		CardView set;

		DataObjectHolder(View itemView) {
			super(itemView);
			image = itemView.findViewById(R.id.setImage);
			check = itemView.findViewById(R.id.setCheck);
			set = itemView.findViewById(R.id.set);

			set.setOnClickListener(v -> onColorClickListener.onColorClick(getAdapterPosition()));
		}
	}

	public void setOnColorClickListener(OnColorClickListener onColorClickListener) {
		this.onColorClickListener = onColorClickListener;
	}

	public SetColorsListAdapter(ArrayList<Color> colors) {
		this.colors = colors;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return colors.get(position).hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.set_color_item_view, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.image.setColorFilter(colors.get(position).getColor());
		holder.check.setColorFilter(colors.get(position).getColor());
		holder.check.setVisibility(colors.get(position).isChecked() ? View.VISIBLE : View.GONE);
	}

	@Override
	public int getItemCount() {
		return colors.size();
	}

	public interface OnColorClickListener {
		void onColorClick(int position);
	}
}