package com.infinitemind.fiszkomania.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.infinitemind.fiszkomania.R;
import com.infinitemind.fiszkomania.models.Color;
import com.infinitemind.fiszkomania.models.Question;

import java.util.ArrayList;

public class QuestionsListAdapter extends RecyclerView.Adapter<QuestionsListAdapter.DataObjectHolder> {
	private ArrayList<Question> questions;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		AppCompatTextView question, answer;

		DataObjectHolder(View itemView) {
			super(itemView);
			question = itemView.findViewById(R.id.question);
			answer = itemView.findViewById(R.id.answer);
		}
	}

	public QuestionsListAdapter(ArrayList<Question> questions) {
		this.questions = questions;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return questions.get(position).hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.question_item_view, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.question.setText(questions.get(position).getQuestion());
		holder.answer.setText(questions.get(position).getAnswer());
	}

	@Override
	public int getItemCount() {
		return questions.size();
	}
}