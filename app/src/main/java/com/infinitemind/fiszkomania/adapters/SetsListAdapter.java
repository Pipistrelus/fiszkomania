package com.infinitemind.fiszkomania.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.infinitemind.fiszkomania.R;
import com.infinitemind.fiszkomania.models.Set;

import java.util.ArrayList;

public class SetsListAdapter extends RecyclerView.Adapter<SetsListAdapter.DataObjectHolder> {
	private OnSetClickListener onSetClickListener;
	private ArrayList<Set> sets;
	private Context context;
	private int width;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		AppCompatTextView title, number;
		ImageView image;
		ViewGroup background;
		CardView set;

		DataObjectHolder(View itemView) {
			super(itemView);
			title = itemView.findViewById(R.id.setTitle);
			number = itemView.findViewById(R.id.setNumber);
			image = itemView.findViewById(R.id.setImage);
			background = itemView.findViewById(R.id.background);
			set = itemView.findViewById(R.id.set);

			background.getLayoutParams().width = width;
			set.setOnClickListener(view -> onSetClickListener.onSetClick(getAdapterPosition()));
		}
	}

	public void setOnSetClickListener(OnSetClickListener onSetClickListener) {
		this.onSetClickListener = onSetClickListener;
	}

	public SetsListAdapter(int width, ArrayList<Set> sets) {
		this.width = width;
		this.sets = sets;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return sets.get(position).hashCode();
	}

	@Override
	public int getItemViewType(int position) {
		return position == getItemCount() - 1 ? 1 : 0;
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(context = parent.getContext()).inflate(R.layout.set_item_view, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.number.setText(getItemViewType(position) == 0 ? context.getResources().getString(R.string.set_number, (position + 1)) : "+\nAdd a set");
		holder.title.setText(sets.get(position).getTitle());
		holder.number.setTextColor(sets.get(position).getColor());
		holder.image.setColorFilter(sets.get(position).getColor());
	}

	@Override
	public int getItemCount() {
		return sets.size();
	}

	public interface OnSetClickListener {
		void onSetClick(int position);
	}
}